export default `
  <h2>Proteção de Dados Pessoais</h2>

  <p>
    1. A PORTO SEGURO (aqui compreendida por todas as empresas pertencentes
    ao seu grupo econômico), tem o compromisso de respeitar e garantir a
    privacidade e a proteção dos dados pessoais dos titulares e por
    isso, declara que o tratamento de dados pessoais se dá para o desempenho
    de su as atividades legais, observando a legislação aplicável sobre
    segurança da informação, privacidade e proteção de dados e demais normas
    setoriais ou gerais sobre o tema.
  </p>

  <p>
    2. A coleta de dados pessoais pode ocorrer de diversas formas, como por
    exemplo: na cotação e/ou contratação de seus diversos produtos e
    serviços, utilizações do site e aplicativos, bem como nas interações com
    os diversos canais de comunicação, mas sempre respeitando os princípios
    finalidade, adequação, necessidade, transparência, livre acesso,
    segurança, prevenção e não discriminação e obrigações legais.
  </p>

  <p>
    3. A PORTO SEGURO implementará as medidas técnicas e organizacionais
    apropriadas para proteger os dados pessoais, levando em conta técnicas
    avançadas disponíveis, o contexto e as finalidades do tratamento. As
    medidas de segurança atenderão as (i) exigências das leis de proteção de
    dados; e (ii) medidas de segurança correspondentes com as boas práticas
    de mercado.
  </p>

  <p>
    4. Os dados pessoais serão, em regra, armazenados pelo tempo que
    perdurará a relação entre as partes. Entretanto, há situações em que
    esses dados deverão ser armazenados além do período de relacionamento e
    essas situações advêm de exigências legais e/ou regulatórias, ou quando
    for necessário para exercer direitos em processos judiciais ou
    administrativos.
  </p>

  <p>
    5. A PORTO SEGURO possui uma Política de Privacidade, a qual encontra-se
    disponível no seguinte
    endereço www.portoseguro.com.br/politica-de-privacidade.
  </p>
`
