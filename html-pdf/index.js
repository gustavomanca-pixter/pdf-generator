var fs = require('fs')
var pdf = require('html-pdf')
var html = fs.readFileSync(`${__dirname}/example.html`, 'utf8')
var options = {
  format: 'Letter',
  paginationOffset: 2,
  border: {
    top: '24px'
  }
}

pdf
  .create(html, options)
  .toFile(`${__dirname}/html-pdf.pdf`, function (err, res) {
    if (err) return console.log(err)
    console.log(res) // { filename: '/app/businesscard.pdf' }
  })
