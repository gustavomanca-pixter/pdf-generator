module.exports = (pdf) => {
  pdf.fontSize('16').fillColor('#4A4FF4').text('BILHETE SEGURO', 48, 120)

  pdf
    .fontSize('12')
    .fillColor('#505050')
    .text('Nr. BILHETE: 01.20.0531.0633250.000.', 48, 148)

  pdf
    .fontSize('12')
    .fillColor('#505050')
    .text(
      'Vigência do contrato: às 24h de 17/08/2020 às 24h de 17/09/2020.',
      48,
      164
    )

  pdf
    .fontSize('12')
    .fillColor('#505050')
    .text(
      'Processo SUSEP Nr. 01.123.456.789 Número do contrato: 0231.125457.163564.6',
      48,
      180
    )
}
