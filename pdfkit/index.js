const PDFKit = require('pdfkit')
const fs = require('fs')

const pdf = new PDFKit()

pdf.rect(0, 0, 1000, 80).fill('#FC4F9E')

pdf.fontSize(20).text('Hello world!', 40, 80, { color: '#FFFFFF' })

pdf.pipe(fs.createWriteStream(`${__dirname}/pdfkit.pdf`))
pdf.end()
