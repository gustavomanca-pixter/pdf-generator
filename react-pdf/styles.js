import { StyleSheet } from '@react-pdf/renderer'

export default StyleSheet.create({
  all: {
    color: '#505050'
  }
})
