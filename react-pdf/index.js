import React from 'react'
import { Page, Document, render, View } from '@react-pdf/renderer'

import Body from './components/Body'
import Header from './components/Header'

import styles from './styles'

const MyDocument = () => (
  <Document>
    <Page size="A4" style={styles.all} wrap>
      <Header title="BLLU Seguro por Assinatura" />
      <Body />
    </Page>
  </Document>
)

render(<MyDocument />, `${__dirname}/react-pdf.pdf`)
