import React from 'react'

import { Image, Text, View } from '@react-pdf/renderer'

import { getImagePath } from '../../../utils'
import styles from './styles'

const Header = ({ title = 'BLLU Seguro por Assinatura' }) => (
  <View style={styles.header}>
    <View style={styles.titleWrapper}>
      <Text style={styles.title}>{title}</Text>
    </View>
    <View style={styles.imagesWrapper}>
      <Image
        src={getImagePath('/bllu.png')}
        style={styles.brand}
        allowDangerousPaths="true"
      />
      <Image
        src={getImagePath('/dots.png')}
        style={styles.dots}
        allowDangerousPaths="true"
      />
    </View>
  </View>
)

export default Header
