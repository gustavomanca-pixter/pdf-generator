import { StyleSheet } from '@react-pdf/renderer'

export default StyleSheet.create({
  header: {
    backgroundColor: '#FC4F9E',
    color: '#FFFFFF',
    display: 'flex',
    flexDirection: 'row',
    height: '80px',
    justifyContent: 'space-between',
    position: 'relative',
    width: '100%'
  },
  titleWrapper: {
    padding: '24px 48px'
  },
  title: {
    fontSize: '20',
    fontWeight: '900'
  },
  imagesWrapper: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: '100%'
  },
  brand: {
    height: '48px',
    margin: '0 24px 0 0',
    width: '80px'
  },
  dots: {}
})
