import Body from './Body'
import Header from './Header'
import HeadingType1 from './HeadingType1'
import HeadingType2 from './HeadingType2'
import HeadingType3 from './HeadingType3'
import TableType1 from './TableType1'
import TableType2 from './TableType2'

export default {
  Body,
  Header,
  HeadingType1,
  HeadingType2,
  HeadingType3,
  TableType1,
  TableType2
}
