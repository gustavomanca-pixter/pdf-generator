import React from 'react'

import { Text, View } from '@react-pdf/renderer'

import styles from './styles'

const HeadingType1 = ({ title, description }) => {
  const allDescriptions = description.split('<br />')

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      {allDescriptions.map((each, index) => (
        <Text key={index} style={styles.description}>
          {each}
        </Text>
      ))}
    </View>
  )
}

export default HeadingType1
