import React from 'react'

import { View } from '@react-pdf/renderer'

import Footer from '../Footer'

import content from '../mock'

import componentsMap from '../index'

import styles from './styles'

const Body = () => (
  <View style={styles.body}>
    {content.map(({ component, ...rest }, index) => {
      const Component = componentsMap[component]

      return <Component {...rest} key={index} />
    })}

    <Footer />
  </View>
)

export default Body
