import { StyleSheet } from '@react-pdf/renderer'

export default StyleSheet.create({
  body: {
    height: '100%',
    padding: '40px'
  }
})
