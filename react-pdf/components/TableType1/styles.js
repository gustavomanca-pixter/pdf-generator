import { StyleSheet } from '@react-pdf/renderer'

export default StyleSheet.create({
  container: {
    margin: '0 0 24px'
  },
  title: {
    color: '#4A4FF4',
    margin: '0 0 12px'
  },
  subtitle: {
    color: '#2B2B2B',
    fontSize: 12,
    margin: '0 0 12px'
  },
  tableWrapper: {},
  tableHeader: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: '0 0 12px'
  },
  tableHeaderTitle: {
    color: '#2B2B2B',
    fontSize: 12
  },
  tableRow: {
    color: '#505050',
    display: 'flex',
    flexDirection: 'row',
    fontSize: 12,
    margin: '0 0 6px'
  },
  tableContentLabel: {
    display: 'flex',
    width: '35%'
  },
  tableContentValue: {
    display: 'flex',
    width: '65%',
    textAlign: 'right'
  }
})
