import React from 'react'
import { Text, View } from '@react-pdf/renderer'

import styles from './styles'

/***
 * Tabela com 2 colunas
 */

const TableType1 = ({ title, subtitle, tableHeader, tableRows }) => (
  <View style={styles.container}>
    <Text style={styles.title}>{title}</Text>
    <Text style={styles.subtitle}>{subtitle}</Text>
    <View style={styles.tableWrapper}>
      <View style={styles.tableHeader}>
        {tableHeader.map((label, index) => (
          <Text style={styles.tableHeaderTitle} key={index}>
            {label}
          </Text>
        ))}
      </View>
      {tableRows.map(({ label, value }, index) => (
        <View style={styles.tableRow} key={index}>
          <Text style={styles.tableContentLabel}>{label}</Text>
          <Text style={styles.tableContentValue}>{value}</Text>
        </View>
      ))}
    </View>
  </View>
)

export default TableType1
