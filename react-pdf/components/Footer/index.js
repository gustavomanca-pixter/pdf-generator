import React from 'react'
import { Text, View } from '@react-pdf/renderer'

import styles from './styles'

const Footer = () => (
  <View style={styles.container}>
    <Text style={styles.tax}>
      EM ATENDIMENTO À REGULAMENTAÇÃO VIGENTE, INFORMAMOS QUE INCIDEM AS
      ALÍQUIOTAS PIS 0,65% E COFINS 4% SOBRE A FORMAÇÃO DE PREÇO.
    </Text>
    <Text style={styles.contact}>
      Atendimento ao Público SUSEP: 0800 021 8484
    </Text>
    <Text style={styles.susep}>
      SUSEP - Superintendência de Seguros privados - Autarquia Federal
      responsável pela fiscalização, normalização e controle dos mercados de
      seguro, previdência complementar aberta, capitalização, resseguro e
      corretagem de seguros.
    </Text>
    <Text style={styles.conditions}>
      As condições contratuais/regulamento desde produto protocolizadas pela
      sociedade/entidade justo à SUSEP poderão ser consultadas no endereço
      eletrônico www.susep.gov.br, de acordo com o número de processo constante
      da apólice/proposta.
    </Text>
  </View>
)

export default Footer
