import { StyleSheet } from '@react-pdf/renderer'

export default StyleSheet.create({
  container: {
    textAlign: 'center',
    padding: '40px'
  },
  tax: {
    fontSize: 12,
    margin: '0 0 24px'
  },
  contact: {
    fontSize: 10,
    color: '#2B2B2B',
    margin: '0 0 8px'
  },
  susep: {
    fontSize: 10,
    margin: '0 0 8px'
  },
  conditions: {
    fontSize: 10,
    color: '#2B2B2B',
    margin: '0 0 8px'
  }
})
