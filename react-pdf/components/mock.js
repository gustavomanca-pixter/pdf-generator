export default [
  {
    title: 'BILHETE SEGURO',
    description:
      'Nr. BILHETE:  01.20.0531.0633250.000.<br />Vigência do contrato: às 24h de 17/08/2020 às 24h de 17/09/2020.<br />Processo SUSEP Nr. 01.123.456.789 Número do contrato: 0231.125457.163564.6',
    component: 'HeadingType1'
  },
  {
    title: 'Correspondência',
    description:
      'MARIO BOSSI<br />DOS DOMINICANOS 980 - BOA VISTA - CURITIBA / PR  - 82540-140',
    component: 'HeadingType1'
  },
  {
    title: 'Cliente',
    data: [
      {
        label: 'Nome',
        value: 'MARIO BOSSI'
      },
      {
        label: 'Endereço',
        value: 'DOS DOMINICANOS 980 - BOA VISTA - CURITIBA / PR  - 82540-140'
      },
      {
        label: 'CPF/CNPJ',
        value: '123.456.789-00'
      }
    ],
    component: 'HeadingType2'
  },
  {
    title: 'Preço do Seguro',
    subtitle: 'Quantidade de itens: 001',
    tableHeader: ['Coberturas', 'Valores em Reais (R$)'],
    tableRows: [
      {
        label:
          'INDENIZAÇÃO INTEGRAL - (colisão, roubo, furto, incêndio, alagamento)',
        value: '20.000,00'
      },
      {
        label: 'COLISÃO PARCIAL - Participação Segurado',
        value: '20.000,00'
      },
      {
        label: 'TERCEIROS',
        value: '20.000,00'
      },
      {
        label: 'ASSISTÊNCIA EMERGENCIAL 400km (listar serviços)',
        value: '20.000,00'
      },
      {
        label: 'VOUCHER TRANSPORTE POR APP',
        value: '20.000,00'
      }
    ],
    component: 'TableType1'
  },
  {
    title: 'Pagamentos',
    tableHeader: [
      'COBRANÇA EM CARTÃO MASTERCARD (R$)',
      'Valores em Reais (R$)'
    ],
    tableRows: [
      {
        label: 'PARCELA: 01'
      },
      {
        label: 'VALOR: 205,78',
        value: '205,78'
      },
      {
        label: 'VENCIMENTO: 18/08/2020'
      }
    ],
    component: 'TableType1'
  },
  {
    title: 'Proteção de Dados Pessoais',
    data: [
      '1. A PORTO SEGURO (aqui compreendida por todas as empresas pertencentes ao seu grupo econômico), tem o compromisso de respeitar e garantir a privacidade e a proteção dos dados pessoais dos titulares e por isso, declara que o tratamento de dados pessoais se dá para o desempenho de su as atividades legais, observando a legislação aplicável sobre segurança da informação, privacidade e proteção de dados e demais normas setoriais ou gerais sobre o tema.',
      '2. A coleta de dados pessoais pode ocorrer de diversas formas, como por exemplo: na cotação e/ou contratação de seus diversos produtos e serviços, utilizações do site e aplicativos, bem como nas interações com os diversos canais de comunicação, mas sempre respeitando os princípios finalidade, adequação, necessidade, transparência, livre acesso, segurança, prevenção e não discriminação e obrigações legais.',
      '3. A PORTO SEGURO implementará as medidas técnicas e organizacionais apropriadas para proteger os dados pessoais, levando em conta técnicas avançadas disponíveis, o contexto e as finalidades do tratamento. As medidas de segurança atenderão as (i) exigências das leis de proteção de dados; e (ii) medidas de segurança correspondentes com as boas práticas de mercado.',
      '4. Os dados pessoais serão, em regra, armazenados pelo tempo que perdurará a relação entre as partes. Entretanto, há situações em que esses dados deverão ser armazenados além do período de relacionamento e essas situações advêm de exigências legais e/ou regulatórias, ou quando for necessário para exercer direitos em processos judiciais ou administrativos.',
      '5. A PORTO SEGURO possui uma Política de Privacidade, a qual encontra-se disponível no seguinte endereço www.portoseguro.com.br/politica-de-privacidade.'
    ],
    component: 'HeadingType3'
  },
  {
    title: 'Importante',
    description:
      'CONSULTE AS CONDIÇÕES GERAIS DO SEU SEGURO EM NOSSO SITE: www.bllu.com.br, OPÇÃO LOGIN/SEGURADO E CLIQUE NA VERSÃO 08/2020 E LEI ATENTAMENTE. VERIFIQUE OS SEUS DADOS PESSOAIS, ITEM SEGURADO, GARANTIAS CONTRATADAS E SEUS RESPECTIVOS VALORES. EM QUALQUER DIVERGÊNCIA, ENTRE EM CONTATO COM SEU CORRETOR E SOLICITE A CORREÇÃO IMEDIATAMENTE, A FIM DE EVITAR PERDA DE DIREITO EM CASO DE INDENIZAÇÃO. A FALTA DE PAGAMENTO DO BILHETE NO PRAZO DEVIDO, ACARRATERÁ O CANCELAMENTO DO CONTRATO.',
    component: 'HeadingType1'
  },
  {
    title: 'Dados do Veículo',
    data: [
      {
        label: 'Item',
        value: '001'
      },
      {
        label: 'Fator Ajuste',
        value: '90%'
      },
      {
        label: 'Modelo',
        value: 'NOVA S10 CAB DUPLA LTZ 2.5 ECOTEC  4x4 AUT.'
      },
      {
        label: 'Ano Fab.',
        value: '19'
      },
      {
        label: 'Placa.',
        value: 'SC/QJA7564'
      },
      {
        label: 'Chassi',
        value: '9BG158MAOKC559008'
      },
      {
        label: 'Tipo  de Utilização',
        value: 'Particular'
      },
      {
        label: 'Classe de Localização',
        value: 'METROPOLITANA DE CURITIBA'
      },
      {
        label: 'Cobertura Básica',
        value: 'Compreensiva'
      },
      {
        label: 'Valor da Franquia (indenização parcial)',
        value: 'R$ xx.xxx,xx'
      },
      {
        label: 'Cláusulas',
        value: '37Y'
      },
      {
        label: 'Código FIPE/TARIFA',
        value: '0000045122113'
      }
    ],
    component: 'HeadingType2'
  },
  {
    subtitle: 'Valores das Franquias (R$)',
    data: [
      {
        label: 'asco',
        value: '3.893,00'
      },
      {
        label: 'Acessórios',
        value: '0,00'
      },
      {
        label: 'Vidro Para-brisa e traseiro',
        value: '350,00'
      },
      {
        label: 'Vidro lateral',
        value: '110,00'
      },
      {
        label: 'Retrovisor',
        value: '490,00'
      },
      {
        label: 'Farol e Lanterna',
        value: '160,00'
      },
      {
        label: 'Farol de Xenônio (item série)',
        value: '1290,00'
      },
      {
        label: 'Lanterna de LED (item série)',
        value: '270,00'
      },
      {
        label: 'Equipamento',
        value: '0,00'
      }
    ],
    component: 'HeadingType2'
  },
  {
    title: 'Garantias',
    tableHeader: ['Valor Coberto (R$)', 'Prêmio Liquido (R$)'],
    tableRows: [
      {
        column1: 'AUTOMOVEL',
        column2: '20.000,00',
        column3: '1.052,22'
      },
      {
        column1: 'RCFV DANOS MATERIAIS',
        column2: '20.000,00',
        column3: '338,70'
      },
      {
        column1: 'RCFV DANOS CORPORAIS',
        column2: '20.000,00',
        column3: '42,78'
      },
      {
        column1: 'VIDROS/RETROV./FAROIS/LANTERNAS',
        column2: '20.000,00',
        column3: '106,87'
      }
    ],
    component: 'TableType2'
  }
]
