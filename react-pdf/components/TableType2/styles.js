import { StyleSheet } from '@react-pdf/renderer'

export default StyleSheet.create({
  container: {
    margin: '0 0 24px'
  },
  title: {
    color: '#4A4FF4',
    display: 'flex',
    width: '50%'
  },
  tableWrapper: {},
  tableHeader: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: '0 0 12px'
  },
  tableHeaderTitle: {
    color: '#2B2B2B',
    display: 'flex',
    fontSize: 12,
    textAlign: 'right',
    width: '25%'
  },
  tableRow: {
    color: '#505050',
    display: 'flex',
    flexDirection: 'row',
    fontSize: 12,
    margin: '0 0 6px'
  },
  column1: {
    display: 'flex',
    width: '50%'
  },
  column2: {
    display: 'flex',
    textAlign: 'right',
    width: '25%'
  },
  column3: {
    display: 'flex',
    textAlign: 'right',
    width: '25%'
  }
})
