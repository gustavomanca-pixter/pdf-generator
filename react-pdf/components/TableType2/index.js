import React from 'react'
import { Text, View } from '@react-pdf/renderer'

import styles from './styles'

/***
 * Tabela com 3 colunas
 */

const TableType2 = ({ title, tableHeader, tableRows }) => (
  <View style={styles.container}>
    <View style={styles.tableWrapper}>
      <View style={styles.tableHeader}>
        <Text style={styles.title}>{title}</Text>
        {tableHeader.map((label, index) => (
          <Text style={styles.tableHeaderTitle} key={index}>
            {label}
          </Text>
        ))}
      </View>
      {tableRows.map(({ column1, column2, column3 }, index) => (
        <View style={styles.tableRow} key={index}>
          <Text style={styles.column1}>{column1}</Text>
          <Text style={styles.column2}>{column2}</Text>
          <Text style={styles.column3}>{column3}</Text>
        </View>
      ))}
    </View>
  </View>
)

export default TableType2
