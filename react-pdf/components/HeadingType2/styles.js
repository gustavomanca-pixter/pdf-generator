import { StyleSheet } from '@react-pdf/renderer'

export default StyleSheet.create({
  container: {
    margin: '0 0 24px'
  },
  title: {
    color: '#4A4FF4',
    margin: '0 0 12px'
  },
  subtitle: {
    color: '#2B2B2B',
    fontSize: 12,
    margin: '0 0 12px'
  },
  description: {
    display: 'flex',
    flexDirection: 'row',
    fontSize: 12,
    lineHeight: 1.4
  },
  label: {
    color: '#2B2B2B',
    display: 'flex'
  },
  value: {
    display: 'flex'
  }
})
