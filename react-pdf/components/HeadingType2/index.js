import React from 'react'

import { Text, View } from '@react-pdf/renderer'

import styles from './styles'

const HeadingType2 = ({ title, subtitle, data }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      {subtitle && <Text style={styles.subtitle}>{subtitle}</Text>}
      {data.map(({ label, value }, index) => (
        <View style={styles.description} key={index}>
          <Text style={styles.label}>
            {label}
            {': '}
          </Text>
          <Text style={styles.value}>{value}</Text>
        </View>
      ))}
    </View>
  )
}

export default HeadingType2
