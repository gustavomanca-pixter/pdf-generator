import { StyleSheet } from '@react-pdf/renderer'

export default StyleSheet.create({
  container: {
    margin: '0 0 24px'
  },
  title: {
    color: '#4A4FF4',
    margin: '0 0 12px'
  },
  text: {
    fontSize: 12,
    lineHeight: 1.4,
    margin: '0 0 20px'
  }
})
