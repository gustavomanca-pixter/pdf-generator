import React from 'react'

import { Text, View } from '@react-pdf/renderer'

import styles from './styles'

const HeadingType3 = ({ title, data }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      {data.map((text, index) => (
        <Text key={index} style={styles.text}>
          {text}
        </Text>
      ))}
    </View>
  )
}

export default HeadingType3
